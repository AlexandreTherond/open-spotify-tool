import json
import os
import sys
import webbrowser
from json.decoder import JSONDecodeError

import spotipy
import spotipy.util as util
from spotipy.oauth2 import SpotifyClientCredentials

# Get the username from terminal
# username = sys.argv[1]
username = "-f"
print("user", username)
scope = 'user-read-private user-read-playback-state user-modify-playback-state'

try:
    token = util.prompt_for_user_token(username,
                                       scope,
                                       client_id='70705a1a2e294d76b2d3c3466c5564a4',
                                       client_secret='ad93af59d78046a8be87723cde952b45',
                                       redirect_uri='https://google.fr')
except (AttributeError, JSONDecodeError):
    os.remove(f".cache-{username}")
    token = util.prompt_for_user_token(username, scope)