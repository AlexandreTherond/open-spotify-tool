from django.shortcuts import render
from django.http import HttpResponse
from .forms import HomeForm
import datetime
import json
import os
import sys
import webbrowser
from json.decoder import JSONDecodeError
from .utils import get_duration_intervals_plot, get_release_date_plot, get_popularity_graph

import spotipy
import spotipy.util as util
from spotipy.oauth2 import SpotifyClientCredentials
import numpy as np
import time

playlist = {}
previous_artist_search = None
previous_song_search = None
header = """<html><nav>
        <a href='/home'>Home</a> | 
        <a href='/connection'>Spotify connection</a> | 
        </nav></html>"""

SCOPES = ["user-library-modify", "user-top-read", "playlist-read-private"]

username = '-f'
scope = SCOPES[2]

token = util.prompt_for_user_token(username,
                                   scope,
                                   client_id='70705a1a2e294d76b2d3c3466c5564a4',
                                   client_secret='ad93af59d78046a8be87723cde952b45',
                                   redirect_uri='http://127.0.0.1:8000/home/')

client_credentials_manager = SpotifyClientCredentials("70705a1a2e294d76b2d3c3466c5564a4",
                                                      "ad93af59d78046a8be87723cde952b45")


def find_song_artist(search_term, group):
    """
    Function searching whether an artist or a song appear in a playlist.

    :param search_term: The term we are searching
    :param group: define if we search a song, 0, or an artist, 1
    :return: dictionary of all the playlist name and song/artist name where search_term appear
    """
    if group == 0:
        group = "songs"
    elif group == 1:
        group = "artists"

    search_term = str.lower(search_term)
    result = {}

    for i in range(len(playlist)):
        playlist_group = list(list(playlist.values())[i][group])
        if any(search_term in piece for piece in playlist_group):
            result_group = []

            for ele in list(playlist.values())[i][group]:
                if search_term in ele:
                    result_group.append(ele)
            result[list(playlist.values())[i]["name"]] = result_group

    return result


def hw_view(request):
    """
    View of the home page.
    Can search artist or song via find_song_artist() function.

    :param request:
    :return: result of the find_song_artist() function
    """
    global previous_artist_search, previous_song_search
    name = "name"
    search_song_term, search_song_result = '', ''
    search_artist_term, search_artist_result = '', ''

    if 'search_song' in request.GET:
        search_song_term = request.GET['search_song']
        search_song_result = find_song_artist(search_song_term, 0)
        previous_song_search = search_song_term
        if previous_artist_search is not None:
            search_artist_result = find_song_artist(previous_artist_search, 1)

    elif 'search_artist' in request.GET:
        search_artist_term = request.GET['search_artist']
        search_artist_result = find_song_artist(search_artist_term, 1)
        previous_artist_search = search_artist_term
        if previous_song_search is not None:
            search_song_result = find_song_artist(previous_song_search, 0)

    return render(request, 'home.html', {'playlist': playlist, 'name': name,
                                         'search_song_term': search_song_term,
                                         'search_song_result': search_song_result,
                                         'search_artist_term': search_artist_term,
                                         'search_artist_result': search_artist_result})


def connection(request):
    """
    View od the connection function to the spotify API

    :param request:
    :return: Spotify username, scope of the session and playlists
    """
    body = """<html><p>
        <h3>Spotify connection</h3>"""

    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

    if token and scope == SCOPES[2]:
        sp = spotipy.Spotify(auth=token)
        sp.trace = False

        # Get playlists name + id
        temp = sp.current_user_playlists()
        results = temp['items']
        while temp['next']:
            temp = sp.next(temp)
            results.extend(temp['items'])

        for item in results:
            playlist[item['tracks']['href'][37:-7]] = {'name': item['name']}

        # Get songs for each playlist
        for ids in playlist.keys():
            pl_id = 'spotify:playlist:%s' % ids
            track = []
            sng_ids = []
            artists_list = []

            for song in sp.playlist_tracks(pl_id, fields='items.track.name')['items']:
                track.append(str.lower(song['track']['name']))
            playlist[ids]["songs"] = track

            for song in sp.playlist_tracks(pl_id, fields='items.track.id')['items']:
                sng_ids.append(song['track']['id'])
            playlist[ids]["songs_ids"] = sng_ids

            # Get artist for each playlist
            for artist in sp.playlist_tracks(pl_id, fields='items.track.artists')['items']:
                if str.lower(artist["track"]["artists"][0]["name"]) not in artists_list:
                    artists_list.append(str.lower(artist["track"]["artists"][0]["name"]))
            playlist[ids]["artists"] = artists_list

        print("###Done###")

    else:
        print("Can't get token for", username)

    # Print playlists
    body += """username: {}<br> scope: {}""".format(username, scope)
    body += """<br><br>
            <p>Current playlists:</p>
            <ol>
            """
    for ele in playlist:
        body += "<li>{}</li>".format(playlist[ele]["name"])

    body += "</ol></p></html>"

    return render(request, 'connection.html', {'username': username, 'scope': scope, 'playlist': playlist})
    # return HttpResponse(header + body)


def playlists_page(request):
    """
    View of the playlist page.
    Only print the playlists

    Playlist is a dictionnary, its key is its unique playlist identifier
        3 values:
            name: The name of the playlist
            songs: Name of evey song in the playlist
            songs_ids: Unique id of each songs
            artists: Name of each artist, 1 artist can represent multiple songs

    :param request:
    :return: page playlists_page.html
    :return: dictionary of playlist
    """
    playlist_id = ''
    chart_duration = ''
    chart_rlse_dte = ''
    chart_popularity = ''

    search = []
    nb_artists = 0
    rlse_dte = []
    duration = []
    popularity = []

    median_rlse = 0
    mean_duration = 0
    total_duration = 0
    mean_popularity = 0

    longest_track = ('', 0)
    shortest_track = ('', 999999)

    if 'playlist_id' in request.GET:
        playlist_id = request.GET['playlist_id']

        if token and scope == SCOPES[2]:
            sp = spotipy.Spotify(auth=token)

        search = sp.playlist(playlist_id)['tracks']['items']

        for track in search:
            try:
                rlse_dte.append(track['track']['album']['release_date'][:4])
            except (ValueError, Exception):
                pass
            duration.append(track['track']['duration_ms'] * 0.001)  # convert ms to s
            popularity.append(track['track']['popularity'])

            if track['track']['duration_ms'] * 0.001 > longest_track[1]:
                longest_track = (track['track']['name'], track['track']['duration_ms'] * 0.001)

            if track['track']['duration_ms'] * 0.001 < shortest_track[1]:
                shortest_track = (track['track']['name'], track['track']['duration_ms'] * 0.001)

        rlse_dte = list(map(int, rlse_dte))

        median_rlse = int(np.median(np.array(rlse_dte)))
        mean_duration = time.strftime('%H:%M:%S', time.gmtime(sum(duration) / len(duration)))
        total_duration = time.strftime('%H:%M:%S', time.gmtime(sum(duration)))
        mean_popularity = sum(popularity) / len(popularity)

        chart_duration = get_duration_intervals_plot(duration)
        chart_rlse_dte = get_release_date_plot(rlse_dte)
        chart_popularity = get_popularity_graph(popularity)

        nb_artists = len(list(set([a['track']['artists'][0]['name'] for a in search])))

    return render(request, 'playlists_page.html', {'playlist': playlist,
                                                   'playlist_id': playlist_id,
                                                   'nb_tracks': len(search),
                                                   'nb_artists': nb_artists,
                                                   'longest_track_name': longest_track[0],
                                                   'longest_track_duration': time.strftime('%H:%M:%S',
                                                                                           time.gmtime(
                                                                                               longest_track[1])),
                                                   'shortest_track_name': shortest_track[0],
                                                   'shortest_track_duration': time.strftime('%H:%M:%S',
                                                                                            time.gmtime(
                                                                                                shortest_track[1])),
                                                   'median_rlse': median_rlse,
                                                   'mean_duration': mean_duration,
                                                   'total_duration': total_duration,
                                                   'mean_popularity': mean_popularity,
                                                   'chart_duration': chart_duration,
                                                   'chart_rlse_dte': chart_rlse_dte,
                                                   'chart_popularity': chart_popularity,
                                                   })
