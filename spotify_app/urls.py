from django.urls import path

from . import views

urlpatterns = [
    path('', views.hw_view, name='hw_view'),
    path('home/', views.hw_view, name='hw_view'),
    path('connection/', views.connection, name='connection'),
    path('playlists/', views.playlists_page, name='playlists'),
]