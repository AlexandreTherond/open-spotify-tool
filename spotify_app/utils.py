import matplotlib.pyplot as plt
import numpy as np
import base64
from io import BytesIO


def get_graph():
    buffer = BytesIO()
    plt.savefig(buffer, format='png')
    buffer.seek(0)
    image_png = buffer.getvalue()
    graph = base64.b64encode(image_png)
    graph = graph.decode('utf-8')
    buffer.close()
    return graph


def get_duration_intervals_plot(duration):
    duration_intervals = {
        30: 0,
        60: 0,  # less than 1 min
        90: 0,
        120: 0,
        150: 0,
        180: 0,
        210: 0,
        240: 0,
        270: 0,
        300: 0,  # between 4:30 and5 min
        330: 0,
        360: 0,
        390: 0,
        420: 0,  # between 6 and 7 min
        450: 0,
        480: 0,  # between 7 and 8 min
        510: 0,  # more than 8 mins
    }

    for ele in duration:
        if ele < 30:
            duration_intervals[30] += 1
        elif ele < 60:
            duration_intervals[60] += 1
        elif ele < 90:
            duration_intervals[90] += 1
        elif ele < 120:
            duration_intervals[120] += 1
        elif ele < 150:
            duration_intervals[150] += 1
        elif ele < 180:
            duration_intervals[180] += 1
        elif ele < 210:
            duration_intervals[210] += 1
        elif ele < 240:
            duration_intervals[240] += 1
        elif ele < 270:
            duration_intervals[270] += 1
        elif ele < 300:
            duration_intervals[300] += 1
        elif ele < 330:
            duration_intervals[330] += 1
        elif ele < 360:
            duration_intervals[360] += 1
        elif ele < 390:
            duration_intervals[390] += 1
        elif ele < 420:
            duration_intervals[420] += 1
        elif ele < 450:
            duration_intervals[450] += 1
        elif ele < 480:
            duration_intervals[480] += 1
        else:
            duration_intervals[510] += 1

    plt.switch_backend('AGG')
    # plt.figure(figsize=(6, 3))
    minutes = {0: 0, 60: 1, 120: 1, 180: 1, 240: 1, 300: 1, 360: 1, 420: 1, 480: 1}
    labels = ["", "1 minute", "2 minute", "3 minute", "4 minute", "5 minute", "6 minute", "7 minute", "8 minute"]

    fig, ax = plt.subplots()
    ax.bar(np.array(list(duration_intervals.keys()))-15, duration_intervals.values(), color='b', width=26)
    ax.bar(list(minutes.keys()), minutes.values(), color='lightcoral', width=2)

    ax.set_title("Tracks duration")
    ax.set_xlabel("Duration splitted in 30s intervals")
    ax.set_ylabel("Number of tracks")
    ax.set(xlim=[0, 550])

    ax.set_xticks(list(minutes.keys()))
    ax.set_xticklabels(labels, rotation=30)

    fig.tight_layout()
    graph = get_graph()
    return graph


def get_release_date_plot(rlse_dte):
    fig, ax = plt.subplots()
    ax.hist(rlse_dte, range(min(rlse_dte), max(rlse_dte) + 1), width=0.8)

    ax.set_title("Repartition of tracks release date")
    ax.set_xlabel("Year")
    ax.set_ylabel("Number of tracks")

    fig.tight_layout()
    graph = get_graph()
    return graph


def get_popularity_graph(popularity):
    fig, ax = plt.subplots()
    ax.hist(np.array(popularity) / 10, range(0, 11), width=0.8)

    ax.set_title("Repartition of popularity")
    ax.set_xlabel("Popularity")
    ax.set_ylabel("Number of tracks")

    fig.tight_layout()
    graph = get_graph()
    return graph
